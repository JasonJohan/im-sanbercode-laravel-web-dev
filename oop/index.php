<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded."<br> <br>"; // "no"

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>"; // "budok"
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded : " . $kodok->cold_blooded."<br>"; // "no"
echo "Jump : " . $kodok->jump("Hop Hop") . "<br> <br>"; // "Hop Hop"

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>"; // "kera sakti"
echo "Legs : " . $sungokong->legs . "<br>"; // 2
echo "cold blooded : " . $sungokong->cold_blooded."<br>"; // "no"
echo "Yell : " . $sungokong->yell("Auooo"); // "Auooo"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>